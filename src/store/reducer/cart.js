const innitialState = {
  cartList: [],
};

const reducers = (state = innitialState, action) => {
  switch (action.type) {
    case "ADD_TO_CART":
      state.cartList = action.payload;
      return { ...state };
    case "INCREASE_QUANTITY":
      state.cartList = action.payload;
      return { ...state };
    case "DECREASE_QUANTITY":
      state.cartList = action.payload;
      return { ...state };
    case "DELETE_FROM_CART":
      state.cartList = action.payload;
      return { ...state };
    default:
      return state;
  }
};
export default reducers;
