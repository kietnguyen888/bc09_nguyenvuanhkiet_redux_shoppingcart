import React, { Component } from "react";
import { connect } from "react-redux";
import ProductItem from "./ProductItem";
class ProductList extends Component {
  displayProduct = () => {
    console.log(this.props.productList);
    return this.props.productList.map((item) => {
      return (
        <div key={item.id} className="col-3">
          <ProductItem prod={item} />
        </div>
      );
    });
  };
  render() {
    return (
      <div className="container">
        <div className="row">{this.displayProduct()}</div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    productList: state.product.productList,
  };
};
export default connect(mapStateToProps)(ProductList);
