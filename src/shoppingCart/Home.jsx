import React, { Component } from "react";
import Cart from "./Cart";
import Detail from "./Detail";
import ProductList from "./ProductList";
import { connect } from "react-redux";
class Home extends Component {
  render() {
    return (
      <div className="container-fluid text-center">
        <h1 className="text-center text-success">Shopping Cart</h1>
        <button
          data-toggle="modal"
          data-target="#modelId"
          className="btn btn-danger mb-3"
        >
          Giỏ hàng
        </button>
        <ProductList />
        <Cart />
        {this.props.selectedProduct && <Detail />}
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    selectedProduct: state.product.selectedProduct,
    cartList: state.cart.cartList,
  };
};
export default connect(mapStateToProps)(Home);
