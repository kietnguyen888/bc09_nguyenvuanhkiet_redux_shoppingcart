import React, { Component } from "react";
import { connect } from "react-redux";
class ProductItem extends Component {
  setSelectedProduct = () => {
    this.props.dispatch({
      type: "SET_SELECTED_PRODUCT",
      payload: this.props.prod,
    });
  };
  addToCart = (prod) => {
    const cloneCart = [...this.props.cartList];
    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === prod.id;
    });

    if (foughtIndex === -1) {
      const cartItem = { product: prod, quantity: 1 };
      cloneCart.push(cartItem);
    } else {
      cloneCart[foughtIndex].quantity++;
    }
    this.props.dispatch({
      type: "ADD_TO_CART",
      payload: cloneCart,
    });
  };
  render() {
    const { name, img } = this.props.prod;
    return (
      <div className="card">
        <img style={{ height: 250, width: "100%" }} src={img} alt="product" />
        <div className="card-body">
          <h4>{name}</h4>
          <button onClick={this.setSelectedProduct} className="btn btn-info">
            Chi tiết
          </button>
          <button
            onClick={() => this.addToCart(this.props.prod)}
            className="btn btn-danger"
          >
            Thêm giỏ hàng
          </button>
        </div>
      </div>
    );
  }
}
const mapStateToProps = (state) => {
  return {
    cartList: state.cart.cartList,
  };
};
export default connect(mapStateToProps)(ProductItem);
