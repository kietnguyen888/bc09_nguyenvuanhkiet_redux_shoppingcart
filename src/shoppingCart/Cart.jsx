import React, { Component } from "react";
import { connect } from "react-redux";
class Cart extends Component {
  handleRenderCart = () => {
    return this.props.cartList.map((item) => {
      const { id, name, img, price } = item.product;
      return (
        <tr key={id}>
          <td>{id}</td>
          <td>
            <img src={img} alt="product" style={{ width: 120 }} />
          </td>
          <td>{name}</td>
          <td>
            <button
              onClick={() => this.decreaseQuantity(id)}
              className="btn btn-info"
            >
              -
            </button>
            <span>{item.quantity}</span>
            <button
              onClick={() => this.increaseQuantity(id)}
              className="btn btn-info"
            >
              +
            </button>
          </td>
          <td>{price}</td>
          <td>{item.quantity * price}</td>
          <td>
            <button
              onClick={() => this.deleteFromCart(id)}
              className="btn btn-danger"
            >
              Xóa
            </button>
          </td>
        </tr>
      );
    });
  };
  increaseQuantity = (id) => {
    const cloneCart = [...this.props.cartList];

    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });

    cloneCart[foughtIndex].quantity++;
    this.props.dispatch({
      type: "INCREASE_QUANTITY",
      payload: cloneCart,
    });
  };

  decreaseQuantity = (id) => {
    const cloneCart = [...this.props.cartList];

    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });
    if (cloneCart[foughtIndex].quantity === 1) {
      return;
    }
    cloneCart[foughtIndex].quantity--;

    this.props.dispatch({
      type: "DECREASE_QUANTITY",
      payload: cloneCart,
    });
  };
  deleteFromCart = (id) => {
    const cloneCart = [...this.props.cartList];
    const foughtIndex = cloneCart.findIndex((item) => {
      return item.product.id === id;
    });
    cloneCart.splice(foughtIndex, 1);
    this.props.dispatch({
      type: "DELETE_FROM_CART",
      payload: cloneCart,
    });
  };

  render() {
    return (
      <div
        className="modal fade"
        id="modelId"
        tabIndex={-1}
        role="dialog"
        aria-labelledby="modelTitleId"
        aria-hidden="true"
      >
        <div className="modal-dialog modal-xl" role="document">
          <div className="modal-content">
            <div className="modal-header">
              <h5 className="modal-title">Giỏ hàng</h5>
              <button
                type="button"
                className="close"
                data-dismiss="modal"
                aria-label="Close"
              >
                <span aria-hidden="true">×</span>
              </button>
            </div>
            <div className="modal-body">
              <table className="table">
                <thead>
                  <tr>
                    <th>Mã sản phẩm</th>
                    <th>Hình Ảnh</th>
                    <th>Tên</th>
                    <th>Số lượng</th>
                    <th>Đơn giá</th>
                    <th>Thành tiền</th>
                  </tr>
                </thead>
                <tbody>{this.handleRenderCart()}</tbody>
              </table>
            </div>
            <div className="modal-footer">
              <button
                type="button"
                className="btn btn-secondary"
                data-dismiss="modal"
              >
                Close
              </button>
            </div>
          </div>
        </div>
      </div>
    );
  }
}

const mapStateToProps = (state) => {
  return {
    cartList: state.cart.cartList,
  };
};
export default connect(mapStateToProps)(Cart);
